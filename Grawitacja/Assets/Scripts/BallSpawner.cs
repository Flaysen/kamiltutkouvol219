﻿using System;
using System.Collections;
using UnityEngine;

public class BallSpawner : MonoBehaviour
{
   public event Action<int> OnBallSpawned;

    [SerializeField] private GameObject _ballPrefab;

    private BallPool _ballPool;
    private int _ballCount = 0;

    private void Awake()
    {
        _ballPool = FindObjectOfType<BallPool>();
    }
    private void Start()
    {
        StartCoroutine(SpawnBallsWithDelay(0.25f));
    }
    private IEnumerator SpawnBallsWithDelay(float delay)
    { 
      while(_ballCount < 250)
      {
          yield return new WaitForSeconds(delay);
          SpawnBall(GetRandomScreenPosition(), 3, Vector3.one, 1);
          _ballCount++;
          OnBallSpawned?.Invoke(_ballCount); 
      }
      StopAllCoroutines();          
    }

    public void SpawnBall(Vector2 position, int gravityFieldSize,
        Vector3 scale, float mass)
    {
        Ball ballToSpawn = _ballPool.Get();
        ballToSpawn.Initialize(position, gravityFieldSize, scale, mass, this);      
    }

    private Vector2 GetRandomScreenPosition()
    {     
        return Camera.main.ViewportToWorldPoint(new Vector2(
          UnityEngine.Random.value,
          UnityEngine.Random.value));                       
    }
}
