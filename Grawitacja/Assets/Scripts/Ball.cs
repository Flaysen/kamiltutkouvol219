﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public bool Collided {get; set;}
    public Rigidbody2D Rigidbody => _rigidbody;
    public int GravityFieldSize => _gravityFieldSize;
    private int _gravityFieldSize;
    private Transform _transform;
    private Rigidbody2D _rigidbody;
    private BallSpawner _ballSpawner;
    private int _connectedBalls;
    
    private  void Awake() 
    {
        _rigidbody =  GetComponent<Rigidbody2D>();  
        _transform = GetComponent<Transform>(); 
    }

    private void FixedUpdate()
    {
        AttractAllBalls();
    }

    private void OnCollisionEnter2D(Collision2D other)  
    {    
        HandleBallCollision(other);
    }

    public void Initialize(Vector2 position, int gravityFieldSize,
        Vector3 scale,  float mass, BallSpawner ballSpawner)
    {
        _transform.position = position;
        _gravityFieldSize = gravityFieldSize;
        _transform.localScale = scale;
        _ballSpawner = ballSpawner;
        _rigidbody.mass = mass;
        Collided = false;
        gameObject.SetActive(true);
    }

    private void AttractAllBalls()
    {
        Ball [] balls = FindObjectsOfType<Ball>();
        foreach(Ball ball in balls)
        {
            if(ball != this)
                Attract(ball);
        }
    }

    private void Attract(Ball attractedBall)
    {
        Rigidbody2D attractedtRigidbody = attractedBall.Rigidbody;

        Vector2 direction = Rigidbody.position - attractedtRigidbody.position;  
        float distance = direction.magnitude;

        if(distance < _gravityFieldSize)
        {
            float forceMagnitude = (Rigidbody.mass * attractedtRigidbody.mass) / Mathf.Pow(distance , 2.0f);
            Vector2 force = direction.normalized * forceMagnitude;
            attractedtRigidbody.AddForce(force);
        }   
    }

    private void HandleBallCollision(Collision2D other)
    {
        Ball otherBall = other.collider.GetComponent<Ball>();
        if (otherBall != null && !otherBall.Collided)
        {
            Collided = true;
            Vector3 scale = otherBall.transform.localScale + _transform.localScale;
            float mass = otherBall.Rigidbody.mass + _rigidbody.mass;
            int gravityFieldSize = otherBall.GravityFieldSize + _gravityFieldSize;
            _ballSpawner.SpawnBall(_transform.position, 3, scale, mass);
        }  
        gameObject.SetActive(false);
    }
}
