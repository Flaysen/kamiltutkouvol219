﻿using UnityEngine;
using UnityEngine.UI;

public class UI_BallCountText : MonoBehaviour
{
    private BallSpawner _ballSpawner;
    private Text _text;

    private void Awake() 
    {
        _ballSpawner = FindObjectOfType<BallSpawner>();
        _text = GetComponent<Text>();
    }

    private void Start() =>  _ballSpawner.OnBallSpawned += UpdateText;
        
    private void UpdateText(int count)
    {
        _text.text = "Ball count: " + count;
    }
   
}
